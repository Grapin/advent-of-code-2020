use std::fs;
use std::collections::HashMap;
use regex::Regex;

fn main() {
    let passports = read_input("src/bin/input/day-4");
    let mut count = 0;
    for passport in &passports {
        if check_passport(&passport) {
            count += 1;
        }
    }
    println!("Found {} passports", passports.len());
    println!("Found {} valid passports", count);
}

fn read_input(file: &str) -> Vec<HashMap<String, String>> {
    let file = match fs::read_to_string(file) {
        Ok(str) => str,
        Err(_) => {panic!("Couldn't read file");},
    };
    // Remove the last '\n' that wouldn't be stripped by the split
    let file = &file[..file.len()-1];
    let passports: Vec<&str> = file.split("\n\n").collect();
    let mut formated_passports = Vec::new();

    //println!("nombre de passport : {}", passports.len());
    for passport in &passports {
        //println!("brut :\n{:?}", passport);
        let passport: Vec<&str> = passport.split(|c| c == ' ' || c == '\n').collect();
        let mut formated_passport: HashMap<String, String> = HashMap::new();
        //println!("nettoyé :\n{:?}", passport);
        for field in &passport {
            let field: Vec<&str> = field.split(':').collect();
            if !formated_passport.contains_key(&field[0].to_string()) {
                formated_passport.insert(field[0].to_string(), field[1].to_string());
            }
            else {
                println!("Passport with two entries\n{:?}", &passport);
            }
        }
        //println!("formaté :\n{:?}", formated_passport);
        formated_passports.push(formated_passport);
    }
    formated_passports
}

fn check_passport(passport: &HashMap<String, String>) -> bool {
    let allowed_fields = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid", "cid"];
    let required_fields = &allowed_fields[..7];
    //let optional_fields = &allowed_fields[7];
    for required in required_fields {
        if passport.contains_key(&required.to_string()) {
            let field = passport.get_key_value(&required.to_string()).unwrap();
            if !check_field(field.0, field.1) {
                return false;
            }
        }
        else { return false; }
    }
    true
}

fn check_field(field: &String, value: &String) -> bool {
    if *field == "byr".to_string(){check_byr(value)}
    else if *field == "iyr".to_string(){check_iyr(value)}
    else if *field == "eyr".to_string(){check_eyr(value)}
    else if *field == "hgt".to_string(){check_hgt(value)}
    else if *field == "hcl".to_string(){check_hcl(value)}
    else if *field == "ecl".to_string(){check_ecl(value)}
    else if *field == "pid".to_string(){check_pid(value)}
    else {true}
}

fn check_byr(value: &String) -> bool {
    let value:i32 = value.parse().unwrap();
    value >= 1920 && value <= 2002 
}

fn check_iyr(value: &String) -> bool {
    let value:i32 = value.parse().unwrap();
    value >= 2010 && value <= 2020
}

fn check_eyr(value: &String) -> bool {
    let value:i32 = value.parse().unwrap();
    value >= 2020 && value <= 2030
}

fn check_hgt(value: &String) -> bool {
    let re = Regex::new(r"(cm|in)$").unwrap();
    let unit = match re.captures(&value) {
        Some(txt) => txt,
        None => {return false;},
    };
    let unit = &unit[0];
    let re = Regex::new(r"^\d+").unwrap();
    let value = re.captures(&value).unwrap();
    let value:i32 = value[0].parse().unwrap();
    if unit == "cm" {
        value >= 150 && value <= 193
    }
    else {
        value >= 59 && value <= 76
    }
}

fn check_hcl(value: &String) -> bool {
    let re = Regex::new(r"^#[0-9a-f]{6}$").unwrap();
    re.is_match(&value)
}

fn check_ecl(value: &String) -> bool {
    let authorized_color = ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"];
    match authorized_color.iter().find(|x| x == &value) {
        Some(_) => true,
        None => false,
    }
}

fn check_pid(value: &String) -> bool {
    let re = Regex::new(r"^[0-9]{9}$").unwrap();
    re.is_match(&value)
}

