use std::fs::File;
use std::io::{self, BufRead};

fn main() {
    let stream = match read_input("src/input/day-9") {
        Ok(vec) => vec,
        Err(_) => {println!("Couldn't read file"); return;}
    };
    let mut checksum = Vec::<u64>::new();
    let preamble_size = 25;
    let mut invalid_key = 0;
    for i in 0..preamble_size {
        for j in i+1..preamble_size {
            checksum.push(stream[i] + stream[j]);
        }
    }
    // Looking for invalid data
    for i in preamble_size..stream.len() {
        //println!("checksum for {}\n\
                //{:?}",
                //stream[i], checksum);
        // First we check that the line is valid
        if let Some(_nbr) = checksum.iter().find(|&&x| x == stream[i]) {
            // Now we compute the new checksum for next iteration
            checksum.drain(..preamble_size-1); // remove old combinaison
            for j in 0..preamble_size-1 {
                let index = (1+j)*(preamble_size-1)-(j*(j+1))/2-1;
                checksum.insert(index, stream[i] + stream[i - preamble_size +j+1]);
            }
        }
        else {
            println!("First invalid number is {} on line {}", stream[i], i+1);
            invalid_key = stream[i];
            break;
        }
    }

    // Looking for contiguous sum
    let mut cum_sum = Vec::<u64>::new();
    for i in 1..stream.len() {
        // Compute the different sum for the i first line
        for j in 0..i {
            if j == i-1 {
                cum_sum.push(stream[i] + stream[j]);
            }
            else {
                cum_sum[j] += stream[i]
            }
        }
        // Check if one of the sum is equal to the invalid key
        if let Some(idx) = cum_sum.iter().position(|&x| x == invalid_key) {
            // we just found the key so i is the end of the sequence
            // now we have to retrieve the start from the index
            println!("start: {}, end: {}, sum: {}", idx, i, stream[idx..i+1].iter().fold(0,|acc, x| acc + x));
            let min = stream[idx..i+1].iter().min().unwrap();
            let max = stream[idx..i+1].iter().max().unwrap();
            let weakness = *min + *max;
            println!("Encryption weakness is: {}", weakness);
            break;
        }
    }
}

fn read_input(file: &str) -> Result<Vec<u64>, io::Error> {
    let file = File::open(file)?;
    let file = io::BufReader::new(file);
    let mut stream = Vec::<u64>::new();
    for line in io::BufReader::new(file).lines() {
        if let Ok(text) = line {
            let line: u64 = text.trim().parse().unwrap();
            stream.push(line);
        }
    }
    Ok(stream)
}
