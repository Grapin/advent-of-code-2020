use std::fs;

struct PasswordRecord {
    min : usize,
    max : usize,
    letter : char,
    password : String,
}

impl PasswordRecord {
    fn tobogan_pass_check(&self) -> bool {
        if self.password.len() < self.min {
            return false
        }
        let first = self.password.chars().nth(self.min-1).unwrap();
        if self.max > self.password.len() {
            return first == self.letter
        }
        else {
            let second = self.password.chars().nth(self.max-1).unwrap();
            return (first == self.letter) ^ (second == self.letter)
        }
    }

    fn sled_rental_pass_check(&self) -> bool {
        if self.password.len() < self.min {
            return false
        }
        let mut count = 0;
        for cursor in self.password.chars() {
            if cursor == self.letter {
                count = count + 1;
            }
            if count > self.max {
                return false
            }
        }
        count >= self.min && count <= self.max
    }
}

fn main() {
    // Read input from file and store it in an vector of Struct
    let database = "src/bin/input/day-2";

    let database = match fs::read_to_string(database) {
        Ok(str) => str,
        Err(_) => {
            println!("Couldn't read file");
            return ;
        }
    };

    let database: Vec<&str> = database.split('\n').collect();

    let mut database_vec = Vec::<PasswordRecord>::new();

    for record in database {
        if record == "" {
            continue;
        }
        let record: Vec<&str> = record.split(' ').collect();
        let policy: Vec<&str> = record[0].split('-').collect();
        let policy: (&str, &str) = (policy[0], policy[1]);
        let number: usize = match policy.0.trim().parse() {
            Ok(num) => num,
            Err(_) => {
                println!("Failed to read {}", policy.0);
                continue;
            }
        };
        let policy: (usize, &str) = (number, policy.1);
        let number: usize = match policy.1.trim().parse() {
            Ok(num) => num,
            Err(_) => {
                println!("Failed to read {}", policy.1);
                continue;
            }
        };
        let policy: (usize, usize) = (policy.0, number);

        let record = PasswordRecord {
            min: policy.0,
            max: policy.1,
            letter: record[1].chars().nth(0).unwrap(),
            password: String::from(record[2]),
        };
        database_vec.push(record);
    }
    let database = database_vec;

    //Check password
    let mut valid_password = 0;
    for record in &database {
        if record.sled_rental_pass_check() {
            valid_password = valid_password + 1;
        }
    }
    println!("Sled Rental policy : {} valid password", valid_password);

    let mut valid_password = 0;
    for record in &database {
        if record.tobogan_pass_check() {
            valid_password = valid_password + 1;
        }
    }
    println!("Tobogan policy : {} valid password", valid_password);
}

