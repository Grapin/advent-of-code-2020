use std::fs;

fn main() {
    // Read input from file and store it in an array
    let expense_report = "src/bin/input/day-1";

    let expense_report = match fs::read_to_string(expense_report) {
        Ok(str) => str,
        Err(_) => {
            println!("Couldn't read file");
            return ;
        }
    };

    let expense_report: Vec<&str> = expense_report.split('\n').collect();

    let mut expense_numbers = Vec::new();

    for entrie in expense_report {
        println!("{}",entrie);
        let entrie: u32 = match entrie.trim().parse() {
            Ok(num) => num,
            Err(_) => {
                println!("Failed to read line : {}", entrie);
                continue;
                }
        };
        expense_numbers.push(entrie);
    }
    let expense_report = expense_numbers;

    // Look for the matching number in the array
    for (i, &first_expense) in expense_report.iter().enumerate() {
        for second_expense in expense_report[i..].iter() {
            if first_expense + second_expense >= 2020 {
                continue;
            }
            for third_expense in expense_report.iter() {
                if first_expense + second_expense + third_expense == 2020 {
                    println!("First expense is {}", first_expense);
                    println!("Second expense is {}", second_expense);
                    println!("Third expense is {}", third_expense);
                    println!("Result : {}", first_expense*second_expense*third_expense);
                }
            }
        }
    }
}
