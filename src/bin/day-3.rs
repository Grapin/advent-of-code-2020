use std::fs;
use std::fmt::Display;

#[derive(Debug)]
enum Tile {
    Open,
    Tree,
}

impl Display for Tile {
    fn fmt(&self, fmt: &mut std::fmt::Formatter) -> std::result::Result<(), std::fmt::Error> {
        match self {
            Tile::Open => write!(fmt, ".",),
            Tile::Tree => write!(fmt, "X",),
        }
    }
}

type Level = Vec<Tile>;

fn main() {
    // Read input from file and store it in an vector of Struct
    let slope = "src/bin/input/day-3";

    let slope = match fs::read_to_string(slope) {
        Ok(str) => str,
        Err(_) => {
            println!("Couldn't read file");
            return ;
        }
    };

    let slope: Vec<String> = slope.lines().map(|row| String::from(row.trim())).collect();
    //for row in &slope {
    //    println!("{:?}", row);
    //}

    let mut cleaned_slope: Vec<Level> = Vec::<Level>::new();
    for row in &slope {
        let mut cleaned_row: Level = Vec::<Tile>::new();
        for tile in &row.chars().collect::<Vec<char>>() {
            if let Some(idtile) = identify_tile(&tile) {
                cleaned_row.push(idtile);
            }
            else {
                println!("Couldn't identify the tile");
            }
        }
        cleaned_slope.push(cleaned_row);
    }
    let slope = cleaned_slope;

    // Let's start checking the slope now
    let mut position1 = 0;
    let mut position2 = 0;
    let mut position3 = 0;
    let mut position4 = 0;
    let mut position5 = 0;
    let mut hit1 = 0;
    let mut hit2 = 0;
    let mut hit3 = 0;
    let mut hit4 = 0;
    let mut hit5 = 0;
    let mut toggle = false;
    for level in &slope[1..] {

        let next = check_next_tile(&position1, &level, 1);
        hit1 = hit1 + match next {
            Tile::Tree => 1,
            Tile::Open => 0,
        };
        position1 = position1+1;

        let next = check_next_tile(&position2, &level, 3);
        hit2 = hit2 + match next {
            Tile::Tree => 1,
            Tile::Open => 0,
        };
        position2 = position2+3;

        let next = check_next_tile(&position3, &level, 5);
        hit3 = hit3 + match next {
            Tile::Tree => 1,
            Tile::Open => 0,
        };
        position3 = position3+5;

        let next = check_next_tile(&position4, &level, 7);
        hit4 = hit4 + match next {
            Tile::Tree => 1,
            Tile::Open => 0,
        };
        position4 = position4+7;

        if toggle {
            let next = check_next_tile(&position5, &level, 1);
            hit5 = hit5 + match next {
                Tile::Tree => 1,
                Tile::Open => 0,
            };
            position5 = position5+1;
            toggle = !toggle;
        }
        else {
            toggle = !toggle;
        }
    }
    println!("You will hit {} tree(s) on trajectory 1", hit1);
    println!("You will hit {} tree(s) on trajectory 2", hit2);
    println!("You will hit {} tree(s) on trajectory 3", hit3);
    println!("You will hit {} tree(s) on trajectory 4", hit4);
    println!("You will hit {} tree(s) on trajectory 5", hit5);
    println!("Resulting in {} total damage", hit1*hit2*hit3*hit4*hit5);
}

fn identify_tile(tile: &char) -> Option<Tile> {
    match tile {
        '.' => Some(Tile::Open),
        '#' => Some(Tile::Tree),
        _ => None,
    }
}

fn check_next_tile(position: &i32, level: &Level, shift: i32) -> Tile {
    match level[((position + shift)%(level.len()as i32)) as usize] {
        Tile::Open => Tile::Open,
        Tile::Tree => Tile::Tree,
    }
}
