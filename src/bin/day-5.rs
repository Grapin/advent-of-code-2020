use std::fs::File;
use std::io::{self, BufRead};

fn main() {
    let input = "src/bin/input/day-5";
    let boarding_passes = match read_input(input) {
        Ok(bp) => bp,
        Err(_) => {return ;}
    };
    let mut max: u32 = 0;
    let mut plane: [[bool; 8]; 128] = [[false; 8]; 128];
    for boarding_pass in &boarding_passes {
        let (row, column) = get_seat(&boarding_pass);
        plane[row as usize][column as usize] = true;
        //println!("row: {}, column: {}, set ID: {}", row, column, row*8 + column);
        if row*8 + column > max {
            max = row*8 + column;
        }
    }
    let mut front = false;
    for (i, row) in plane.iter().enumerate() {
        for (j, seat) in row.iter().enumerate() {
            if *seat == false && front{
                if j == 7 {
                    if plane[(i+1)%128][0] == true {
                        println!("Seat {},{} is yours (ID: {})", i, j, i*8+j);
                    }
                }
                else {
                    if row[j+1] == true {
                        println!("Seat {},{} is yours (ID: {})", i, j, i*8+j);
                    }
                }
            }
            else if *seat == true && !front {
                front = !front;
            }
        }
    }
    println!{"highest set ID : {}", max};
}

fn get_seat(boarding_pass: &String) -> (u32, u32) {
    let boarding_pass: Vec<char> = boarding_pass.chars().collect();
    //println!("{:?}", &boarding_pass);
    let row: String = boarding_pass[..7].iter().map(|x| match x { 'R'=> '1', 'B'=>'1', _=>'0'}).collect();
    //println!("{:?}", &row);
    let row: u32 = u32::from_str_radix(row.as_str(), 2).unwrap();
    let column: String = boarding_pass[7..].iter().map(|x| match x { 'R'=> '1', 'B'=>'1', _=>'0'}).collect();
    //println!("{:?}", &column);
    let column: u32 = u32::from_str_radix(column.as_str(), 2).unwrap();
    (row, column)
}

fn read_input(file: &str) -> Result<Vec<String>, io::Error> {
    let file = File::open(file)?;
    let mut boarding_passes = Vec::new();
    let file = io::BufReader::new(file);
    for line in io::BufReader::new(file).lines() {
        if let Ok(boarding_pass) = line {
            boarding_passes.push(boarding_pass);
        }
    }
    Ok(boarding_passes)
}
