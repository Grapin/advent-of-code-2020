use std::fs::File;
use std::io::{self, BufRead};
use std::collections::HashMap;

fn main() {
    let input = "src/bin/input/day-6";
    let group_survey= match read_input(input) {
        Ok(input) => input,
        Err(_) => {return ;}
    };
    let mut summary = Vec::new();
    for group in group_survey {
        let mut sum = HashMap::new();
        sum.insert('#',group.len());
        for survey in group {
            for question in survey.chars() {
                let count = sum.entry(question).or_insert(0);
                *count += 1;
            }
        }
        summary.push(sum);
    }
    let anyone = &summary.iter()
        .map(|x| x.len()-1).sum::<usize>();

    let mut everyone: usize = 0;
    for mut sum in summary {
        let max = sum.remove(&'#').unwrap();
        for answer in sum.values() {
            if *answer == max {
                everyone += 1;
            }
        }
    }

    println!("Sum of question answered by anyone: {:?}", anyone);
    println!("Sum of question answered by everyone: {:?}", everyone);
}

fn read_input(file: &str) -> Result<Vec<Vec<String>>, io::Error> {
    let file = File::open(file)?;
    let file = io::BufReader::new(file);
    let mut group_surveys= Vec::new();
    let mut group = Vec::<String>::new();
    for line in io::BufReader::new(file).lines() {
        if let Ok(survey) = line {
            if survey != "" {
                group.push(survey);
            }
            else {
                group_surveys.push(group);
                group = Vec::<String>::new();
            }
        }
    }
    // We have to push the last group here because there is no \n
    // at the end of input.
    group_surveys.push(group);
    Ok(group_surveys)
}
