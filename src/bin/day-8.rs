use std::fs::File;
use std::io::{self, BufRead};
use std::io::{Error, ErrorKind};

struct CmdLine {
    cmd: String,
    arg: isize,
}

fn main() {
    let input = "src/input/day-8";
    let instructions = match read_input(input) {
        Ok(vec) => vec,
        Err(_) => {println!("Couldn't read file");return ;}
    };
    let mut accumulator: isize = 0;
    let mut current_line: usize = 0;
    let mut skip_nth: usize = 0;
    let mut current_nth: usize = 0;
    let mut line_read: Vec<usize> = Vec::new();
    let eof = instructions.len();
    let mut allow_skip = false;
    while  current_line < eof {
        line_read.push(current_line);
        if instructions[current_line].cmd == String::from("acc") {
            accumulator += instructions[current_line].arg;
            current_line +=1;
        }
        else if instructions[current_line].cmd == String::from("jmp") {
            if current_nth != skip_nth || !allow_skip {
                if instructions[current_line].arg >= 0 {
                    current_line += instructions[current_line].arg as usize;
                }
                else {
                    current_line -= (-instructions[current_line].arg) as usize;
                }
            }
            // We change the instruction to a nop
            else {
                current_line += 1;
            }
            current_nth += 1;
        }
        else if instructions[current_line].cmd == String::from("nop") {
            if current_nth != skip_nth || !allow_skip {
                current_line += 1;
            }
            // We change the instruction to a jmp
            else {
                if instructions[current_line].arg >= 0 {
                    current_line += instructions[current_line].arg as usize;
                }
                else {
                    current_line -= (-instructions[current_line].arg) as usize;
                }
            }
            current_nth += 1;
        }
        // We will check if the next line as already been read at the end
        // Here, we check if we looped on command.
        if let Some(_nbr) = line_read.iter().find(|&&x| x == current_line) {
            // if we have change an instruction then it wasn't the good one,
            // we restart to try changing the next one.
            if current_nth >= skip_nth {
                if !allow_skip {
                    allow_skip = true;
                    println!("By running the raw instruction, we loop at line {}\n\
                                the current value of the accumulator is {}",
                                current_line, accumulator);
                }
                current_line = 0;
                current_nth = 0;
                accumulator = 0;
                line_read = Vec::new();
                skip_nth += 1;
            }
            // no instruction was changed, we have already try every possible one
            else {
                break;
            }
        }
    }
    println!("End of file reached by changing an instruction\n\
                The value of the accumulator is {}", accumulator);
}

fn read_input(file: &str) -> Result<Vec<CmdLine>, io::Error> {
    let file = File::open(file)?;
    let file = io::BufReader::new(file);
    let mut instructions = Vec::<CmdLine>::new();
    for line in io::BufReader::new(file).lines() {
        if let Ok(text) = line {
            let mut line = text.split_whitespace();
            let cmd = match line.next() {
                Some(arg) => arg,
                None => return Err(Error::new(ErrorKind::Other,"Can't read command")),
            };
            let cmd = String::from(cmd);
            let arg = match line.next() {
                Some(arg) => arg,
                None => return Err(Error::new(ErrorKind::Other,"Can't read command")),
            };
            let arg: isize = arg.parse().unwrap();
            instructions.push(CmdLine {cmd, arg});
        }
    }
    // We have to push the last group here because there is no \n
    // at the end of input.
    Ok(instructions)
}
